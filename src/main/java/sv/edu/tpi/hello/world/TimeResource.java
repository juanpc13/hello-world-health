/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.tpi.hello.world;

import java.time.Instant;
import java.util.Date;
import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.eclipse.microprofile.health.Health;
import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;

/**
 *
 * @author jcpleitez
 */
@Health
@ApplicationScoped
@Path("time")
public class TimeResource implements HealthCheck{
    
    @Override
    public HealthCheckResponse call() {
        return HealthCheckResponse.named("ping").up().withData("Juan", "Shobe").build();
    }
    
    
    @GET
    @Produces({MediaType.APPLICATION_JSON + "; charset=utf-8"})
    public Date time(){
        Date d = Date.from(Instant.now());
        return d;
    }
    
}
