/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.tpi.hello.world;

import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.eclipse.microprofile.health.Health;
import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import sv.edu.tpi.hello.world.entity.Car;
import sv.edu.tpi.hello.world.entity.CarService;

/**
 *
 * @author jcpleitez
 */
@Health
@ApplicationScoped
@Path("cars")
public class CarResource implements HealthCheck{
    
    private CarService carService = new CarService();
    
    @GET
    @Produces({MediaType.APPLICATION_JSON + "; charset=utf-8"})
    public List<Car> allCars(){
        return carService.createCars(10);
    }

    @Override
    public HealthCheckResponse call() {
        return HealthCheckResponse.named("ping").up().withData("Juan2", "Shobe2").build();
    }
    
}
